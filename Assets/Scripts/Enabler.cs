﻿using UnityEngine;
using System.Collections;

public class Enabler : MonoBehaviour
{

    public Behaviour toEnable;

    public void Enable()
    {
        toEnable.enabled = true;
    }

    public void Disable()
    {
        toEnable.enabled = false;
    }
}
