﻿using UnityEngine;
using System.Collections;

public class DamageOnTrigger : MonoBehaviour
{

    public int damage = 1;

    public void OnTriggerEnter2D(Collider2D other)
    {
        var health = other.GetComponent<Health>();

        if (health != null)
        {
            health.ChangeHealth(-damage);
        }
    }
}
