﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public interface IDeadHandler : IEventSystemHandler
{
    void OnDied(string deathType);
}
