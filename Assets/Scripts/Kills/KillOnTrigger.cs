﻿using UnityEngine;
using System.Collections;

public class KillOnTrigger : MonoBehaviour
{

    public string deathType = "squish";

    public void OnTriggerEnter2D(Collider2D other)
    {
        var kd = other.GetComponent<KillDeader>();

        if (kd != null)
        {
            kd.Die(deathType);
        }
    }

    public void OnTriggerStay2D(Collider2D other)
    {
        var kd = other.GetComponent<KillDeader>();

        if (kd != null)
        {
            kd.Die(deathType);
        }
    }
}
