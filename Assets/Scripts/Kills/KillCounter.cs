﻿using UnityEngine;
using System.Collections;

public class KillCounter : MonoBehaviour, IDeadHandler
{
    public void OnDied(string deathType)
    {
        GameManager.Instance.AddKill();
    }

}
