﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class KillDisplayer : MonoBehaviour
{

    private Slider slider;

    // Use this for initialization
    void Start()
    {
        slider = GetComponent<Slider>();
    }

    public void OnEnable()
    {
        GameManager.Instance.GotKill.AddListener(UpdateValue);
        GameManager.Instance.LevelUp.AddListener(UpdateMinMax);
    }

    public void OnDisable()
    {

        GameManager.Instance.GotKill.RemoveListener(UpdateValue);
        GameManager.Instance.LevelUp.RemoveListener(UpdateMinMax);
    }

    void UpdateValue()
    {
        slider.value = GameManager.Instance.Kills;
    }

    void UpdateMinMax()
    {
        if (GameManager.Instance.Level < GameManager.Instance.killsToLevel.Length - 1)
        {
            slider.maxValue = GameManager.Instance.killsToLevel[GameManager.Instance.Level + 1];
            slider.minValue = GameManager.Instance.killsToLevel[GameManager.Instance.Level];
        }
        else
        {
            slider.minValue = 0;
            slider.maxValue = 1;
            slider.value = 1;
        }
    }
}
