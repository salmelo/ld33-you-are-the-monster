﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class KillDeader : MonoBehaviour
{
    public string[] deaths;
    public GameObject[] gibs;

    Dictionary<string, GameObject> deathGibs;

    // Use this for initialization
    void Start()
    {
        if (deaths.Length != gibs.Length)
        {
            Debug.LogError("Deaths and gibs must be same length.");
            Destroy(this);
            return;
        }

        deathGibs = new Dictionary<string, GameObject>();

        for (int i = 0; i < deaths.Length; i++)
        {
            deathGibs[deaths[i]] = gibs[i];
        }

        deaths = null;
        gibs = null;
    }

    public void Die(string type)
    {
        var gib = deathGibs[type];

        Instantiate(gib, transform.position, transform.rotation);

        UnityEngine.EventSystems.ExecuteEvents.Execute<IDeadHandler>(gameObject, null, (x, y) => x.OnDied(type));

        Destroy(gameObject);
    }
}
