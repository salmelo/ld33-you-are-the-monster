﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{

    public string moveAxis = "Vertical";
    public string turnAxis = "Horizontal";

    public float speed = 2;
    public float turnSpeed = 45;

    private Animator animator;

    // Use this for initialization
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        float move = Input.GetAxis(moveAxis) * speed * Time.deltaTime;
        float turn = -Input.GetAxis(turnAxis) * turnSpeed * Time.deltaTime;

        transform.Rotate(0, 0, turn);

        transform.Translate(move, 0, 0);

        animator.SetBool("Walking", move != 0 || turn != 0);

    }
}
