﻿
using UnityEngine;
using System.Collections;

public class GameResetter : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        ScoreManager.Instance.Score = 0;
        GameManager.Instance.Reset();
        Destroy(gameObject);
    }
}
