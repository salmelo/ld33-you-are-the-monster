﻿using UnityEngine;
using System.Collections;

public class Leap : CooldownAbility
{

    public float scale = 1.2f;
    public float distance = 2;
    public float speed = 5;

    public float stunRadius = 1;
    public float stunDuration = 2;

    public float killRadius = .5f;

    public Behaviour[] disable;

    public GameObject FX;

    protected override void UseAbility()
    {
        DoStun();

        StartCoroutine(LeapRoutine());
    }

    private void DoStun()
    {
        if (FX != null)
        {
            Instantiate(FX, transform.position, transform.rotation);
        }

        //Debug.DrawRay(transform.position, Vector3.up * stunRadius, Color.red, 3);

        foreach (var hit in Physics2D.OverlapCircleAll(transform.position, stunRadius))
        {
            var stun = hit.GetComponent<Stunnable>();

            if (stun != null)
            {
                stun.Stun(stunDuration);
            }
        }
    }

    IEnumerator LeapRoutine()
    {

        var oldScale = transform.localScale;

        foreach (var b in disable)
        {
            b.enabled = false;
        }

        var target = transform.position + transform.right * distance;
        var time = distance / speed;
        float t = 0;

        while (transform.position != target)
        {
            transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);

            t += Time.deltaTime;
            var scaleFactor = t / time - .5f;
            scaleFactor = 1 - Mathf.Sign(scaleFactor) * 2 * scaleFactor;

            transform.localScale = oldScale * Mathf.Lerp(1, scale, scaleFactor);

            yield return null;
        }

        transform.localScale = oldScale;
        foreach (var b in disable)
        {
            b.enabled = true;
        }

        foreach (var hit in Physics2D.OverlapCircleAll(transform.position, killRadius))
        {
            var kd = hit.GetComponent<KillDeader>();

            if (kd != null)
            {
                kd.Die("squish");
            }
        }

        DoStun();
    }
}
