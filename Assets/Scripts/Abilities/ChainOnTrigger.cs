﻿using UnityEngine;
using System.Collections;

public class ChainOnTrigger : MonoBehaviour
{

    public float chainRange = 2;
    public int maxChains = 2;

    public LayerMask layers = -1;

    private int chains;

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (chains >= maxChains)
        {
            Destroy(gameObject);
            return;
        }

        chains += 1;

        Vector2 bestDir = Vector2.zero;
        float bestDist = float.PositiveInfinity;

        foreach (var c in Physics2D.OverlapCircleAll(other.transform.position, chainRange, layers))
        {
            if (c != other)
            {
                var dir = c.transform.position - transform.position;
                var dist = dir.sqrMagnitude;
                if (dist < bestDist)
                {
                    bestDir = dir;
                    bestDist = dist;
                }
            }
        }

        if (bestDir != Vector2.zero)
        {
            transform.rotation = Utils.LookRotation2D(bestDir);
        }
        else
        {
            transform.rotation = Quaternion.Euler(0, 0, Random.Range(0, 360f));
        }
    }
}
