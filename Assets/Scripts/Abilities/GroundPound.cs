﻿using UnityEngine;
using System.Collections;

public class GroundPound : CooldownAbility
{

    public float stunDuration = 3;
    public float stunRadius = 2;
    public GameObject FX;
    public string animTrigger = "PoundGround";

    public float killRadius = .5f;

    private Animator anim;

    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    protected override void UseAbility()
    {
        foreach (var hit in Physics2D.OverlapCircleAll(transform.position, killRadius))
        {
            var kd = hit.GetComponent<KillDeader>();

            if (kd != null)
            {
                kd.Die("squish");
            }
        }

        //Debug.DrawRay(transform.position, Vector3.up * stunRadius, Color.red, 3);

        foreach (var hit in Physics2D.OverlapCircleAll(transform.position, stunRadius))
        {
            var stun = hit.GetComponent<Stunnable>();

            if (stun != null)
            {
                stun.Stun(stunDuration);
            }
        }

        if (FX != null)
        {
            Instantiate(FX, transform.position, transform.rotation);
            anim.SetTrigger(animTrigger);
        }
    }
}
