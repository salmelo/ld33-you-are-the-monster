﻿using UnityEngine;
using System.Collections;

public class ProjectileAbility : CooldownAbility
{

    public GameObject projectilePrefab;
    public Transform barrel;


    // Use this for initialization
    void Start()
    {
        if (barrel == null)
        {
            barrel = transform;
        }
    }

    protected override void UseAbility()
    {
        Instantiate(projectilePrefab, barrel.position, barrel.rotation);
    }
}
