﻿using UnityEngine;
using System.Collections;

public class CooldownAbility : MonoBehaviour
{
    public string buttonName = "Jump";

    public float cooldown = 4;

    protected float cd;
    public float CooldownLeft { get { return cd; } }

    // Update is called once per frame
    void Update()
    {
        cd -= Time.deltaTime;

        if (Input.GetButtonDown(buttonName) && cd <= 0 && !GameManager.Instance.Paused)
        {

            cd = cooldown;

            UseAbility();
        }
    }

    protected virtual void UseAbility() { }
}
