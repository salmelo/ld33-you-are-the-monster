﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AbilityButton : MonoBehaviour
{

    public CooldownAbility ability;
    public Image imageTarget;

    // Update is called once per frame
    void Update()
    {
        var fill = 1 - Mathf.Clamp(ability.CooldownLeft / ability.cooldown, 0, 1);
        imageTarget.fillAmount = fill;
    }
}
