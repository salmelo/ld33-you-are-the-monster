﻿using UnityEngine;
using System.Collections;

public class SnapRotateRandomly : MonoBehaviour
{
    public float frequencyMin = .2f;
    public float frequencyMax = 1;

    private float timer;
    private float freq;

    void Start()
    {
        freq = Random.Range(frequencyMin, frequencyMax);
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (timer >= freq)
        {
            timer = 0;
            freq = Random.Range(frequencyMin, frequencyMax);

            transform.rotation = Quaternion.Euler(0, 0, Random.Range(0, 360));
        }
    }
}
