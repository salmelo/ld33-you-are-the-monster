﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Spawner : MonoBehaviour
{

    public int maxEnemies = 10;
    public int maxCivvies = 30;

    public int civMinCluster = 3;
    public int civMaxCluster = 7;

    public int initialCivviesMin = 1;
    public int initialCivviesMax = 5;

    public float minSpawnDelay = .5f;
    public float maxSpawnDelay = 4;

    public GameObject[] civvies;
    public GameObject[] enemies;
    public GameObject[] doodads;

    public List<GameObject> availableEnemies;

    private int currentBiggestEnemy = -1;

    private float camWidth;
    private float camHeight;

    public static Spawner instance;

    // Use this for initialization
    void Start()
    {
        instance = this;

        camHeight = Camera.main.orthographicSize;
        camWidth = Camera.main.orthographicSize / Screen.height * Screen.width;

        StartCoroutine(SpawnCivvies());
        //StartCoroutine(SpawnEnemies());

        SpawnDoodads();

        availableEnemies = new List<GameObject>();
    }

    public void StartSpawningEnemies()
    {
        StartCoroutine(SpawnEnemies());
    }

    public void AddNextEnemy()
    {
        currentBiggestEnemy += 1;
        if (currentBiggestEnemy < enemies.Length)
        {
            availableEnemies.Add(enemies[currentBiggestEnemy]);
        }
    }

    private void SpawnDoodads()
    {
        var container = (new GameObject("Doodads")).transform;

        var count = Random.Range(100, 10000);

        for (int i = 0; i < count; i++)
        {
            SpawnCamp(Random.insideUnitCircle * 100, doodads, container);
        }
    }

    private IEnumerator SpawnEnemies()
    {
        while (true)
        {
            if (GameObject.FindGameObjectsWithTag("Enemy").Length < maxEnemies)
            {
                SpawnCamp(PointOffCam(), availableEnemies);
            }

            yield return new WaitForSeconds(Random.Range(minSpawnDelay, maxSpawnDelay));
        }
    }

    Vector2 PointOffCam()
    {
        Vector2 min = new Vector2();
        Vector2 max = new Vector2();

        if (Random.value > .5f)
        {
            min.x = -camWidth - 2;
            max.x = -camWidth - .3f;
        }
        else
        {
            min.x = camWidth + .3f;
            max.x = camWidth + 2;
        }

        if (Random.value > .5f)
        {
            min.y = -camWidth - 2;
            max.y = -camWidth - .3f;
        }
        else
        {
            min.y = camWidth + .3f;
            max.y = camWidth + 2;
        }

        return new Vector2(Random.Range(min.x, max.x), Random.Range(min.y, max.y));
    }

    Vector2 PointOffCenter()
    {
        var dir = Random.insideUnitCircle;
        var normal = dir.normalized;

        return dir * camHeight + normal;
    }

    void SpawnCamp(Vector2 point, IList<GameObject> dudes, Transform container = null)
    {
        var count = Random.Range(civMinCluster, civMaxCluster);

        for (int i = 0; i < count; i++)
        {
            var p = point + Random.insideUnitCircle * .5f;

            var obj = Instantiate(dudes[Random.Range(0, dudes.Count)], p, Quaternion.Euler(0, 0, Random.Range(0f, 360f)));

            if (container != null)
            {
                ((GameObject)obj).transform.parent = container;
            }
        }
    }

    IEnumerator SpawnCivvies()
    {
        int k = Random.Range(initialCivviesMin, initialCivviesMax);
        for (int i = 0; i < k; i++)
        {
            SpawnCamp(PointOffCenter(), civvies);
        }

        while (true)
        {
            if (GameObject.FindGameObjectsWithTag("Civvie").Length < maxCivvies)
            {
                SpawnCamp(PointOffCam(), civvies);
            }

            yield return new WaitForSeconds(Random.Range(minSpawnDelay, maxSpawnDelay));
        }
    }

}
