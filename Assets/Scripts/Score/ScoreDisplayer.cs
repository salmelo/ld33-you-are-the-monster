﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreDisplayer : MonoBehaviour
{

    private Text text;

    // Use this for initialization
    void Start()
    {
        text = GetComponent<Text>();
    }

    void OnEnable()
    {
        ScoreManager.Instance.ScoreChanged.AddListener(UpdateText);
    }

    public void OnDisable()
    {
        ScoreManager.Instance.ScoreChanged.RemoveListener(UpdateText);
    }

    void UpdateText()
    {
        if (text != null)
        {
            text.text = ScoreManager.Instance.Score.ToString();
        }
    }
}
