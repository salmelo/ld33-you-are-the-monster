﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class ScoreManager : Singleton<ScoreManager>
{

    private int score;

    public int Score
    {
        get
        {
            return score;
        }
        set
        {
            if (score != value)
            {
                score = value;
                ScoreChanged.Invoke();
            }
        }
    }

    public UnityEvent ScoreChanged = new UnityEvent();
    
}
