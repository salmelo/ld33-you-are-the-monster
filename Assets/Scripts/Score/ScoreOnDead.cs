﻿using UnityEngine;
using System.Collections;
using System;

public class ScoreOnDead : MonoBehaviour, IDeadHandler
{
    public int points = 10;

    public void OnDied(string deathType)
    {
        ScoreManager.Instance.Score += points;
    }
}
