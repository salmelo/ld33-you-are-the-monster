﻿using UnityEngine;
using System.Collections;

public class ResetRotation : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        transform.rotation = Quaternion.identity;
    }
}
