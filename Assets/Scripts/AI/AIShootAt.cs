﻿using UnityEngine;
using System.Collections;
using System;

public class AIShootAt : MonoBehaviour
{

    public Transform target;
    public Transform barrel;
    public float targettingMargin = 15;

    public Transform bulletPrefab;
    public float frequency = .7f;

    private float lastShot = float.NegativeInfinity;

    void Start()
    {
        if (barrel == null)
        {
            barrel = transform;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (target != null)
        {
            float angle = Vector2.Angle(transform.up, target.position - barrel.position);

            if (angle <= targettingMargin)
            {
                Shoot();
            }
        }
        else
        {
            target = GameManager.Player;
        }
    }

    private void Shoot()
    {
        if (Time.time - lastShot > frequency)
        {
            var bullet = Instantiate(bulletPrefab);

            bullet.rotation = barrel.rotation;
            bullet.position = barrel.position;

            lastShot = Time.time;
        }
    }
}
