﻿using UnityEngine;
using System.Collections;

public class AIRunAway : MonoBehaviour
{

    public Transform target;
    public float distance = 1;
    public float delayMin = .2f;
    public float delayMax = 1;

    public float turnSpeed = 20;
    public float moveSpeed = 1.25f;
    public float moveFacingMargin = 20;

    private bool running = false;
    private bool seen = false;

    // Update is called once per frame
    void Update()
    {
        if (target != null)
        {
            if (running)
            {
                var localTarget = transform.InverseTransformPoint(target.position);

                float angle = Mathf.Atan2(localTarget.x, localTarget.y);

                if (angle != 0)
                {
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, transform.rotation * Quaternion.Euler(0, 0, angle), turnSpeed * Time.deltaTime);
                }

                if (Mathf.Abs(angle) <= moveFacingMargin)
                {
                    transform.Translate(0, moveSpeed * Time.deltaTime, 0);
                }
            }
            else if (!seen)
            {

                var distSqr = ((Vector2)target.position - (Vector2)transform.position).sqrMagnitude;

                if (distSqr <= distance * distance)
                {
                    Invoke("StartRunning", Random.Range(delayMin, delayMax));
                    seen = true;
                }
            }
        }
        else
        {
            target = GameManager.Player;
        }
    }

    void StartRunning()
    {
        running = true;
    }
}
