﻿using UnityEngine;
using System.Collections;

public class AITurnTowards : MonoBehaviour
{
    public Transform target;

    public float speed = 30;
    public float range = 3;

    // Update is called once per frame
    void Update()
    {
        if (target != null)
        {
            var localTarget = transform.InverseTransformPoint(target.position);

            float angle = -Mathf.Atan2(localTarget.x,localTarget.y);

            //if (localTarget.y == 0)
            //{
            //    angle = localTarget.x < 0 ? 90 : -90;
            //}
            //else
            //{
            //    angle = -Mathf.Sign(localTarget.y) * Mathf.Atan(localTarget.x / localTarget.y);
            //}

            if (angle != 0)
            {
                var distSqr = ((Vector2)target.position - (Vector2)transform.position).sqrMagnitude;

                if (distSqr <= range * range)
                {
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, transform.rotation * Quaternion.Euler(0, 0, angle), speed * Time.deltaTime);
                }
            }
        }
        else
        {
            target = GameManager.Player;
        }
    }
}
