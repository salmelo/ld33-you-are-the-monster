﻿using UnityEngine;
using System.Collections;

public class AIMoveClose : MonoBehaviour
{
    public Transform target;
    public float moveMargin = 15;

    public float speed = 3;
    public float distance = 2;

    // Update is called once per frame
    void Update()
    {
        if (target != null)
        {
            Vector2 dir = target.position - transform.position;
            float angle = Vector2.Angle(transform.up, dir);

            if (angle <= moveMargin)
            {
                var point = (Vector2)target.position - dir.normalized * distance;
                transform.position = Vector2.MoveTowards(transform.position, point, speed * Time.deltaTime);
            }
        }
        else
        {
            target = GameManager.Player;
        }
    }
}
