﻿using UnityEngine;
using System.Collections;

public class Pauser : MonoBehaviour
{
    public void Pause()
    {
        GameManager.Instance.Pause();
    }

    public void UnPause()
    {
        GameManager.Instance.UnPause();
    }

}
