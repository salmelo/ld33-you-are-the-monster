﻿using UnityEngine;
using System.Collections;
using System;

public class Stunnable : MonoBehaviour
{

    public Behaviour[] disable;

    private bool stunned;
    public bool Stunned { get { return stunned; } }

    private float stunnedTimer;

    // Update is called once per frame
    void Update()
    {
        if (stunned)
        {
            stunnedTimer -= Time.deltaTime;
            if (stunnedTimer <= 0)
            {
                UnStun();
            }
        }
    }

    public void Stun(float duration)
    {
        stunned = true;
        stunnedTimer = duration;
        foreach (var b in disable)
        {
            b.enabled = false;
        }
    }

    public void UnStun()
    {
        stunned = false;
        foreach (var b in disable)
        {
            b.enabled = true;
        }
    }
}
