﻿using UnityEngine;
using System.Collections;

public class DisableAfterFixedUpdates : MonoBehaviour
{
    public int count = 2;

    private int remaining;

    void OnEnable()
    {
        remaining = count;
        StartCoroutine(Delay());
    }

    IEnumerator Delay()
    {
        while (remaining > 0)
        {
            remaining -= 1;
            yield return new WaitForFixedUpdate();
        }

        gameObject.SetActive(false);
    }
}
