﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{
    public Transform target;

    public float verticalRoom = .1f;
    public float horizontalRoom = .2f;

    public float verticalSpeed = 2f;
    public float horizontalSpeed = 2;

    public float verticalChunk = 1;
    public float horizontalChunk = 1.5f;

    private float vertSpace;
    private float horiSpace;

    private Camera camera;

    private bool[] moving = new bool[2];

    // Use this for initialization
    void Start()
    {
        camera = GetComponent<Camera>();

        vertSpace = camera.orthographicSize - camera.orthographicSize * verticalRoom;

        var horSize = camera.orthographicSize / Screen.height * Screen.width;

        horiSpace = horSize - horSize * horizontalRoom;
    }

    // Update is called once per frame
    void Update()
    {
        if (target != null)
        {
            var dist = target.position - transform.position;

            if (!moving[0])
            {
                if (dist.x > horiSpace)
                {
                    StartCoroutine(Move(0, transform.position.x + horizontalChunk, horizontalSpeed));
                }
                else if (dist.x < -horiSpace)
                {
                    StartCoroutine(Move(0, transform.position.x - horizontalChunk, horizontalSpeed));
                }
            }

            if (!moving[1])
            {
                if (dist.y > vertSpace)
                {
                    StartCoroutine(Move(1, transform.position.y + verticalChunk, verticalSpeed));
                }
                else if (dist.y < -vertSpace)
                {
                    StartCoroutine(Move(1, transform.position.y - verticalChunk, verticalSpeed));
                }
            }
        }
    }

    IEnumerator Move(int axis, float target, float speed)
    {
        moving[axis] = true;

        while (transform.position[axis] != target)
        {
            var pos = transform.position;
            pos[axis] = Mathf.MoveTowards(pos[axis], target, speed * Time.deltaTime);
            transform.position = pos;
            yield return null;
        }

        moving[axis] = false;
    }

}
