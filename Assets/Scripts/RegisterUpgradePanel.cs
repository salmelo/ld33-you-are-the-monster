﻿using UnityEngine;
using System.Collections;

public class RegisterUpgradePanel : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        GameManager.Instance.upgradePanel = gameObject;

        foreach (var c in GetComponentsInChildren<RegisterLevelUpChoice>())
        {
            c.Register();
        }

        gameObject.SetActive(false);
    }
}
