﻿using UnityEngine;
using System.Collections;

public class FadeAway : MonoBehaviour
{

    public float fadeTime = 6;

    private float time;

    private new SpriteRenderer renderer;

    void Start()
    {
        renderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        var alpha = Mathf.InverseLerp(fadeTime, 0, time);
        renderer.color = new Color(1, 1, 1, alpha);

        if (alpha == 0)
        {
            Destroy(gameObject);
        }

        time += Time.deltaTime;
    }
}
