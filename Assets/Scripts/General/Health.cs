﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class Health : MonoBehaviour
{

    public int startingHealth = 100;

    private int health;
    private int maxHealth;

    public int CurrentHealth { get { return health; } }
    public int MaxHealth { get { return maxHealth; } }

    public HealthEvent died;
    public IntEvent healthChanged;

    // Use this for initialization
    void Start()
    {
        maxHealth = startingHealth;
        health = startingHealth;
    }

    public void ChangeHealth(int amount)
    {
        int old = health;

        health += amount;
        if (health > maxHealth) health = maxHealth;
        else if (health < 0) died.Invoke(this);

        if (old != health)
        {
            healthChanged.Invoke(health);
        }
    }

    public void ChangeMaxHealth(int amount)
    {
        maxHealth += amount;
    }
}
