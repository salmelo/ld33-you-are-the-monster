﻿using UnityEngine;
using System.Collections;

public class DestroyInvisible : MonoBehaviour
{
    public void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
