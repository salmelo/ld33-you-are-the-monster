﻿using UnityEngine;
using System.Collections;

public class DestroyOnTrigger2D : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D other)
    {
        Destroy(gameObject);
    }
}
