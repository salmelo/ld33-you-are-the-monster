﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Utils
{

    public static Quaternion LookRotation2D(Vector2 forward, bool rightIsForward = false)
    {
        float angle = Mathf.Atan2(forward.y, forward.x) * Mathf.Rad2Deg;

        if (!rightIsForward) angle -= 90;

        return Quaternion.AngleAxis(angle, Vector3.forward);
    }


    private static Dictionary<Transform, Rigidbody2D> rigidbodyCache = new Dictionary<Transform, Rigidbody2D>();
    public static void MoveWithRigidbody(this Transform transform, Vector3 newPos)
    {

        Rigidbody2D rigidbody;

        if (!rigidbodyCache.TryGetValue(transform, out rigidbody))
        {
            rigidbody = transform.GetComponent<Rigidbody2D>();
            rigidbodyCache.Add(transform, rigidbody);
        }

        if (rigidbody)
        {
            rigidbody.MovePosition(newPos);
        }
        else
        {
            transform.position = newPos;
        }
    }

    public static void RotateWithRigidbody(this Transform transform, float angle)
    {
        RotateWithRigidbody(transform,Quaternion.AngleAxis(angle + transform.eulerAngles.z,Vector3.forward));
    }

    public static void RotateWithRigidbody(this Transform transform, Quaternion rotation)
    {
        Rigidbody2D rigidbody;

        if (!rigidbodyCache.TryGetValue(transform, out rigidbody))
        {
            rigidbody = transform.GetComponent<Rigidbody2D>();
            rigidbodyCache.Add(transform, rigidbody);
        }

        if (rigidbody)
        {
            rigidbody.MoveRotation(rotation.eulerAngles.z);
        }
        else
        {
            transform.rotation = rotation;
        }
    }
}
