﻿using UnityEngine;
using UnityEngine.Events;
using System;

[Serializable]
public class HealthEvent : UnityEvent<Health> { }

[Serializable]
public class IntEvent : UnityEvent<int> { }

[Serializable] public class FloatEvent : UnityEvent<float> { }