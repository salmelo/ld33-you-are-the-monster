﻿using UnityEngine;
using System.Collections;

public class IntToFloat : MonoBehaviour
{
    public FloatEvent converted;

    public void Convert(int val)
    {
        converted.Invoke(val);
    }
}