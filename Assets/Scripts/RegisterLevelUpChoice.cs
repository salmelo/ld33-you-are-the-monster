﻿using UnityEngine;
using System.Collections;

public class RegisterLevelUpChoice : MonoBehaviour
{
    public int level;

    // Use this for initialization
    void Start()
    {
        Register();
    }

    public void Register()
    {
        GameManager.Instance.levelUpChoices[level] = gameObject;
        gameObject.SetActive(false);
        Destroy(this);
    }
}
