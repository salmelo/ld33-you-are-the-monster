﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

public class GameManager : Singleton<GameManager>
{

    private bool firstEnemy;

    public static Transform Player;

    private int kills;

    public int Kills { get { return kills; } }

    private int level = 0;

    public int Level { get { return level; } }

    public int[] killsToLevel = { 0, 5, 10 }; //, 20, 30, 50 };

    public UnityEvent LevelUp = new UnityEvent();
    public UnityEvent GotKill = new UnityEvent();

    bool paused = false;
    public bool Paused { get { return paused; } }

    public Dictionary<int, GameObject> levelUpChoices = new Dictionary<int, GameObject>();
    public GameObject upgradePanel;

    // Use this for initialization
    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    public void OnEnable()
    {
        ScoreManager.Instance.ScoreChanged.AddListener(WatchScore);
    }

    public void OnDisable()
    {

        ScoreManager.Instance.ScoreChanged.RemoveListener(WatchScore);
    }

    void WatchScore()
    {
        if (ScoreManager.Instance.Score > 50 && !firstEnemy)
        {
            Spawner.instance.AddNextEnemy();
            Spawner.instance.StartSpawningEnemies();
            firstEnemy = true;
        }
        else
        {
            // todo more enemies
        }
    }

    public void AddKill()
    {
        kills += 1;
        if (level < killsToLevel.Length - 1 && kills >= killsToLevel[level + 1])
        {
            level += 1;
            LevelUp.Invoke();

            GameObject choice;
            if (levelUpChoices.TryGetValue(level, out choice))
            {
                choice.SetActive(true);
                upgradePanel.SetActive(true);
                Pause();
            }
            else
            {
                Debug.LogWarningFormat("No ability choices for level {0}", level);
            }
        }

        GotKill.Invoke();
    }

    public void Pause()
    {
        paused = true;
        Time.timeScale = 0;
    }

    public void UnPause()
    {
        paused = false;
        Time.timeScale = 1;
    }

    public void Reset()
    {
        kills = 0;
        level = 0;
        paused = false;
        Time.timeScale = 1;
        firstEnemy = false;
    }
}
