﻿using UnityEngine;
using System.Collections;

public class DestroyStayOffCam : MonoBehaviour
{

    public float offCamTime = 30;
    public float moveOffCamTime = 5;

    private bool visible;
    private float timeTillGone;

    void Start()
    {
        timeTillGone = offCamTime;
    }

    // Update is called once per frame
    void Update()
    {
        if (!visible)
        {
            timeTillGone -= Time.deltaTime;
            if (timeTillGone <= 0)
            {
                Destroy(gameObject);
            }
        }
    }

    public void OnBecameInvisible()
    {
        visible = false;
        timeTillGone = moveOffCamTime;
    }

    public void OnBecameVisible()
    {
        visible = true;
    }
}
